package pers.mobian.springboot01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//标注这是一个Spring Boot的应用
@SpringBootApplication
public class Springboot01Application {

    public static void main(String[] args) {
        //启动Spring Boot项目，run为静态方法，通过反射调用
        SpringApplication.run(Springboot01Application.class, args);
    }

}
