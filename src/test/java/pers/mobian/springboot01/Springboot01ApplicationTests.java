package pers.mobian.springboot01;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pers.mobian.springboot01.pojo.Dog;
import pers.mobian.springboot01.pojo.Person;

@SpringBootTest
class Springboot01ApplicationTests {

   // @Autowired
    //private Person person;

    @Autowired
    private Dog dog;

    @Test
    void contextLoads() {
        //System.out.println(person);
        System.out.println(dog);
    }

}
